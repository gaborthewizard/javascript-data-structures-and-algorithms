function Node(data) {
  this.data = data;
  this.next = null;
}

Node.prototype = {
  constructor: Node,
  appendToTail: function(data) {
    var end = new Node(data),
        n = this;
    while (n.next !== null) {
      n = n.next;
    }
    n.next = end;
  },
  deleteNode: function(head, d) {
    var n = head;

    if (n.data === d) {
      return head.next; /* moved head */
    }

    while (n.next !== null) {
      if (n.next.data === d) {
        n.next = n.next.next;
        return head; /* head didn't change */
      }
      n = n.next;
    }
    return head;
  }
};