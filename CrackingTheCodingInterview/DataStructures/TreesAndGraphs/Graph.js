'use strict';

function Graph() {
  this.adjacencyList = [];
  this.adjacencyMatrix = [];
}

Graph.prototype = {
  constructor: Graph,
  /** Graphs (Directed and Undirected) **/
  depthFirstSearch: function() {

  },
  breadthFirstSearch: function() {

  },
  /** biDirectional Search **/
};