'use strict';

/**
 * Helper: Helper functions
 * @constructor
 */
function Helper() {
}

Helper.prototype = {
  constructor: Helper,
  visit: function(node) {
    console.log(node.key);
  },
  getRandomArbitrary: function(min, max) {
    return Math.random() * (max - min) + min;
  },
  getRandomInt: function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
  },
  generateRandomArray: function(min, max, n) {
    var arr = [];

    for (var i = 0; i < n; i++) {
      var num = this.getRandomInt(min, max);
      arr.push(num);
    }

    return arr;
  },
};

/**
 * Tree Node: The node used when building trees
 * @param data
 * @constructor
 */
function TreeNode(key, data) {
  this.key = key;
  this.data = data;
  this.left = null;
  this.right = null;
}

/**
 * Tree: All general tree related functions
 * @constructor
 */
function Tree() {
  this.root = null;
  this.helper = new Helper();
}

Tree.prototype = {
  constructor: Tree,
  /** Tree Types: Implemented using nodes for more flexibility */
  getBinaryTree: function(options, arr) {
    var min = options.min || 1,
        max = options.max || 100,
        numOfNodes = (typeof arr !== typeof undefined && arr.length - 1) ||
            options.nodes || this.helper.getRandomInt(5, 10),
        numRandom = (typeof arr !== typeof undefined && arr[0]) ||
            this.helper.getRandomInt(min, max),
        root = new TreeNode(numRandom);

    for (var i = 0; i < numOfNodes; i++) {
      var curr = root,
          key = (typeof arr !== typeof undefined && arr[i + 1]) ||
              this.helper.getRandomInt(min, max),
          newNode = new TreeNode(key);

      while (true) {
        var dir = this.helper.getRandomInt(0, 2);

        if (dir === 0) { // left
          if (curr.left === null) {
            curr.left = newNode;
            break;
          }
          curr = curr.left;
        } else { // right
          if (curr.right === null) {
            curr.right = newNode;
            break;
          }
          curr = curr.right;
        }
      }
    }

    return root;
  },
  getBinarySearchTree: function() {
    return new Tree.BinarySearchTree();
  },
  getCompleteBinaryTree: function() {
    // To Implement
  },
  getFullBinaryTree: function() {
    // To Implement
  },
  getPerfectBinaryTree: function() {
    // To Implement
  },
  /** Tree Traversals **/
  inOrderTraversal: function(node, callback) {
    if (node !== null) {
      this.inOrderTraversal(node.left, callback);
      typeof callback === 'function' ?
          callback(node.key) :
          this.helper.visit(node);
      this.inOrderTraversal(node.right, callback);
    }
  },
  preOrderTraversal: function(node, callback) {
    if (node !== null) {
      typeof callback === 'function' ?
          callback(node.key) :
          this.helper.visit(node);
      this.preOrderTraversal(node.left, callback);
      this.preOrderTraversal(node.right, callback);
    }
  },
  postOrderTraversal: function(node, callback) {
    if (node !== null) {
      this.postOrderTraversal(node.left, callback);
      this.postOrderTraversal(node.right, callback);
      typeof callback === 'function' ?
          callback(node.key) :
          this.helper.visit(node);
    }
  },
  levelOrderTraversal: function() {
    // To Implement
  },
  verticalOrderTraversal: function() {
    // To Implement
  },
  /** Min and Max heaps: Implemented using arrays, since more memory efficient */
  /** Tries (Prefix Trees) **/
};

/**
 * BinarySearchTree: Binary search tree functions
 * @constructor
 */
Tree.BinarySearchTree = function() {
  this.root = null;
  this.helper = new Helper();
};

Tree.BinarySearchTree.prototype = {
  constructor: Tree.BinarySearchTree,
  generate: function(options, arr) {
    var min = options.min || 1,
        max = options.max || 100,
        numOfNodes = (typeof arr !== typeof undefined && arr.length - 1) ||
            options.nodes || this.helper.getRandomInt(5, 10),
        root = null;

    if (typeof arr !== typeof []) {
      arr = this.helper.generateRandomArray(min, max, numOfNodes);
    }

    for (var i in arr) {
      root = this.insert(root, new TreeNode(arr[i]));
    }

    return root;
  },
  searchRecursive: function(node, key) {
    if (node === null || key === node.key) {
      return node;
    } else if (key < node.key) {
      return this.searchRecursive(node.left, key);
    } else {
      return this.searchRecursive(node.right, key);
    }
  },
  searchIterative: function(node, key) {
    while (node !== null && key !== node.key) {
      if (key < node.key) {
        node = node.left;
      } else {
        node = node.right;
      }
    }
    return node;
  }
  ,
  getMinimum: function(root) {
    while (root.left !== null) {
      root = root.left;
    }
    return root;
  },
  getMaximum: function(root) {
    while (root.right !== null) {
      root = root.right;
    }
    return root;
  },
  insert: function(root, node) {
    try {
      if (root !== null && !(root instanceof TreeNode)) {
        throw new EvalError('root must be null or an instance of TreeNode.');
      }
      if (!(node instanceof TreeNode)) {
        throw new EvalError('node must be an instance of TreeNode.');
      }
    } catch (err) {
      console.error(err);
    }

    var temp = null,
        curr = root;

    while (curr !== null) {
      temp = curr;
      if (node.key < curr.key) { // go left
        curr = curr.left;
      } else { // go right
        curr = curr.right;
      }
    }

    if (temp === null) { // empty tree
      root = node;
    } else if (node.key < temp.key) { // insert left
      temp.left = node;
    } else { // insert right
      temp.right = node;
    }

    return root;
  },
  delete: function(root, node) {
    if (node.left === null) {
      this.transplant(root, node, node.right);
    } else if (node.right === null) {
      this.transplant(root, node, node.left);
    } else {
      var temp = this.getMinimum(node.right);
      if (temp !== node) {
        this.transplant(root, temp, temp.right);
        temp.right = node.right;
        temp.right = temp;
        this.transplant(root, node, temp);
        temp.right = node.left;
        temp.left = temp;
      }
    }
    return root;
  },
  getSuccessor: function() {
    // To Implement
  },
  transplant: function(root, nodeU, nodeV) {
    if (nodeU === null) {
      root = nodeV;
    } else if (nodeU === nodeU.left) {
      nodeU.left = nodeV;
    } else {
      nodeU.right = nodeV;
    }

    if (nodeV !== null) {
      nodeV = nodeU;
    }

    return root;
  },
};