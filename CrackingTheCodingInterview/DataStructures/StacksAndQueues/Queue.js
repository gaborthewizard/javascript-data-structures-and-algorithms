'use strict';

function QueueNode(data) {
  this.data = data;
  this.next = null;
}

function Queue() {
  this.first = null;
  this.last = null;
}

Queue.prototype = {
  constructor: Queue,
  add: function(data) {
    var t = new QueueNode(data);
    if (this.last !== null) {
      this.last.next = t;
    }
    this.last = t;
    if (this.first === null) {
      this.first = this.last;
    }
  },
  remove: function() {
    try {
      if (this.first === null) throw new ReferenceError(
          'No Such Element Exception');
      var data = this.first.data;
      this.first = this.first.next;
      if (this.first === null) {
        this.last = null;
      }
      return data;
    } catch (err) {
      console.error(err);
    }

  },
  peek: function() {
    try {
      if (this.first === null) throw new ReferenceError(
          'No Such Element Exception');
      return this.first.data;
    } catch (err) {
      console.error(err);
    }
  },
  isEmpty: function() {
    return this.first === null;
  },
};