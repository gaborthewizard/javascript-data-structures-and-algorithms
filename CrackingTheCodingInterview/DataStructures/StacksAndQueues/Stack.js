'use strict';

function StackNode(data) {
  this.data = data;
  this.next = null;
}

function Stack() {
  this.top = null;
}

Stack.prototype = {
  constructor: Stack,
  pop: function() {
    try {
      if (this.top === null) throw new EvalError('Empty Stack Exception');
      var item = this.top.data;
      this.top = this.top.next;
      return item;
    } catch (err) {
      console.error(err);
    }
  },
  push: function(item) {
    var t = new StackNode(item);
    t.next = this.top;
    this.top = t;
  },
  peek: function() {
    try {
      if (this.top === null) throw new EvalError('Empty Stack Exception');
      return this.top.data;
    } catch (err) {
      console.error(err);
    }
  },
  isEmpty: function() {
    return this.top === null;
  },
};