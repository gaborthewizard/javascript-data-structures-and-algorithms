/**
* https://www.hackerrank.com/contests/projecteuler/challenges/euler001/problem
 *
 * This is the correct solution in JavaScript, but since JavaScript only supports
 * 53 bit integers, the solution cannot pass test cases that are bigger... so I
 * had to write out a Java Solution (instead of bringing in a big number library)
 *
 *
 import java.io.*;
 import java.util.*;
 import java.text.*;
 import java.math.*;
 import java.util.regex.*;

 public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            BigInteger three = getSum(n,3);
            BigInteger five = getSum(n,5);
            BigInteger fifteen = getSum(n,15);
            System.out.println( ((three.add(five)).subtract(fifteen)).divide(new BigInteger("2")) );
        }
    }

    public static BigInteger getSum(int n, int a) {
        BigInteger num = BigInteger.valueOf((n-1)/a);
        BigInteger bigA = BigInteger.valueOf(a);
        return num.multiply(bigA.add(num.multiply(bigA)));
    }
}

 */


var tests = [
  {input: 1, output: 0},
  {input: 3, output: 0},
  {input: 5, output: 3},
  {input: 4, output: 3},
  {input: 6, output: 8},
  {input: 13, output: 45},
  {input: 10, output: 23},
  {input: 100, output: 2318},
  {input: 1000000, output: 233333166668},
];

for (var i in tests) {
  var n = tests[i].input,
      answer = getSum(n,3) + getSum(n,5) - getSum(n, 15);

  if (answer === tests[i].output) {
    console.log("Correct: "+answer);
  } else {
    console.log("Incorrect: "+answer+" was expecting: "+tests[i].output);
  }
}

function getSum(n, a) {
  var num = Math.floor((n-1)/a);
  return num * (a+num*a)/2;
}