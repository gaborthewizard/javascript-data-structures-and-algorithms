/**
 * https://www.hackerrank.com/challenges/minimum-absolute-difference-in-an-array/problem
 */

function minimumAbsoluteDifference(n, arr) {
  var n = arr.length,
      diff = [];

  arr = arr.sort();

  for (var i = 0; i < (n - 1); i++) {
    diff.push(Math.abs(arr[i] - arr[i + 1]));
  }

  return Math.min.apply(null, diff);
}