function countWordsInString(s) {
  var count = 1;

  for (var char in s) {
    if (s[char] === s[char].toUpperCase()) {
      count++;
    }
  }

  return count;
}