/**
 * https://www.hackerrank.com/challenges/funny-string/problem
 */

function funnyString(s) {
  var map = genCharMapObject(),
      reverse = s.split("").reverse().join("");

  for (var j = 0; j < s.length-1; j++) {
    var char1 = s.charAt(j),
        char2 = s.charAt(j+1),
        char3 = reverse.charAt(j+1),
        char4 = reverse.charAt(j);

    if ( Math.abs(map[char2] - map[char1]) !== Math.abs(map[char4] - map[char3]) )  {
      return "Not Funny";
    }
  }

  return "Funny";
}

function genCharMapObject() {
  var obj = {};
  for (var i = 0; i < 26; ++i) {
    var char = String.fromCharCode(97 + i);
    obj[char] = i + 1;
  }
  return obj;
}