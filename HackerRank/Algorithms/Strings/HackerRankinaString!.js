/**
 * https://www.hackerrank.com/challenges/hackerrank-in-a-string/problem
 */

function checkHackerRank(s) {
  var stack = "hackerrank".split("").reverse(),
      n = s.length;

  for(var j = 0; j < n; j++) {
    if (stack[stack.length-1] === s[j]) {
      stack.pop();
    }
    if (stack.length < 1) {
      return "YES";
    }
  }

  return "NO";
}