/**
 * https://www.hackerrank.com/challenges/two-characters/problem
 */

var n = 10,
    s = "beabeefeab";

maxLen(n, s);

function maxLen(n, s){
  // Complete this function
  var chars = getUniqueChars(s);

  for (var char in chars) {
    var shortS = s.replace(chars[char], ""),
        check = checkPattern(shortS);
    console.log(check);
    if (check > 0) {

    }
  }
  console.log(chars);
}

function getUniqueChars(s) {
  var chars = [];

  for (var char in s) {
    if (chars.indexOf(s[char]) < 0){
      chars.push(s[char]);
    }
  }

  return chars;
}

function checkPattern(s) {
  var n = s.length,
      letterOne = s[0],
      letterTwo = s[1];

  for (var i = 2; i < n-1; i++) {
    if (letterOne !== s[i] || letterTwo !== s[i+1]) {
      return 0;
    }
  }

  return n;
}