/**
 * https://www.hackerrank.com/challenges/pangrams/problem
 */

function isPangram(s) {
  var str = s.trim().toLowerCase().split('').sort(),
      n = str.length,
      alphabet = genCharMapArray().reverse();

  str = str.filter(function(value) {
    return value.trim() != '';
  });

  for (var i = 0; i < n; i++) {
    if (str[i] === alphabet[alphabet.length - 1]) {
      alphabet.pop();
    }
  }

  return alphabet.length < 1 ? 'pangram' : 'not pangram';
}

function genCharMapArray() {
  var arr = [];
  for (var i = 0; i < 26; ++i) {
    var char = String.fromCharCode(97 + i);
    arr.push(char);
  }
  return arr;
}