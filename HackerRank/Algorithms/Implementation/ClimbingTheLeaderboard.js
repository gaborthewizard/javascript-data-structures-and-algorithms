/**
 * https://www.hackerrank.com/challenges/climbing-the-leaderboard/problem
 **/

function climbingTheLeaderboard(n, scores, m, alice) {
  var scoresUnique = [];
  // De-dup scores
  scoresUnique.push(scores[0]);
  for (var k = 1; k < n; k++) {
    if (scores[k] < scoresUnique[scoresUnique.length - 1]) {
      scoresUnique.push(scores[k]);
    }
  }

  var rank = scoresUnique.length + 1, // Start at last place
      prevScore = 0,
      j = scoresUnique.length - 1;

  for (var i = 0; i < m; i++) {
    // If alice's score hasn't changed, we assume the rankings are the same
    if (alice[i] > prevScore) {
      prevScore = alice[i];
      // Take the current score and see how many rankings it has jumped
      while (alice[i] >= scoresUnique[j]) {
        j--;
        rank--;
      }
      console.log(rank);
    } else {
      console.log(rank);
    }
  }
}