/**
 https://www.hackerrank.com/challenges/the-birthday-bar/problem
 **/

function solve(n, s, d, m){
  var start = 0,
      end = m-1,
      differentWays = 0;

  while (end < n) {
    var sum = 0;
    for (var i = start; i < (end+1); i++) {
      sum += s[i];
    }
    if (sum === d) {
      differentWays++;
    }
    start++;
    end++;
  }

  return differentWays;
}