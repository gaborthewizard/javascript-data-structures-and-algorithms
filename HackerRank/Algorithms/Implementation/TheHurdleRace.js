/**
 * https://www.hackerrank.com/challenges/the-hurdle-race/problem
 **/

function minimumNumberOfMagicBeverages(n, k, height) {
  var maxHeight = height[0];
  for (var i = 1; i < n; i++) {
    if (height[i] > maxHeight) {
      maxHeight = height[i];
    }
  }
  return (maxHeight - k) > 0 ? maxHeight - k : 0;
}

