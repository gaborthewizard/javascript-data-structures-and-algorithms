/**
 * https://www.hackerrank.com/challenges/migratory-birds/problem
 **/

function migratoryBirds(n, ar) {
  var set = [0,0,0,0,0],
      maxIndex = 0,
      maxValue = 0;

  for (var i = 0; i < n; i++) {
    set[ar[i]-1]++;
  }

  for (var j in set) {
    if (set[j] > maxValue) {
      maxIndex = j;
      maxValue = set[j];
    }
  }

  return parseInt(maxIndex)+1;
}